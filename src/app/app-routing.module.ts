import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DriverComponent } from './pages/driver/driver.component';
import { VehiclesListComponent } from './pages/vehicles-list/vehicles-list.component';

const routes: Routes = [{ path: 'driverPage', component: DriverComponent },
{ path: 'vehiclesPage', component: VehiclesListComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
