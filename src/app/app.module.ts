import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from './material/material.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VehiclesListComponent } from './pages/vehicles-list/vehicles-list.component';
import { AddVehiclesComponent } from './pages/add-vehicles/add-vehicles.component';
import { MatConfirmDialogComponent } from './mat-confirm-dialog/mat-confirm-dialog.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { AngularFireModule } from '@angular/fire/compat';
import { DriverComponent } from './pages/driver/driver.component';

export const firebaseConfig =  {
  projectId: 'sfminiproject',
  appId: '1:505679757371:web:48958b1cc547e390061c1e',
  databaseURL: 'https://sfminiproject-default-rtdb.firebaseio.com',
  storageBucket: 'sfminiproject.appspot.com',
  apiKey: 'AIzaSyDGWQImAP2HgRCecD60a_kVL1xSbJE2CaM',
  authDomain: 'sfminiproject.firebaseapp.com',
  messagingSenderId: '505679757371',
  measurementId: 'G-3Z0TKWRFPM',
}

@NgModule({
  declarations: [
    AppComponent,
    VehiclesListComponent,
    AddVehiclesComponent,
    MatConfirmDialogComponent,
    DriverComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
